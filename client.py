import socket

from kivy.app import App
from kivy.graphics import Color, Ellipse
from kivy.input.providers.mouse import MouseMotionEvent
from kivy.uix.widget import Widget


class TouchInput(Widget):

    def __init__(self, conn: socket, resolution, **kwargs):
        super().__init__(**kwargs)
        self.conn = conn
        self.resolution = resolution

    def draw(self, touch):
        Color(0.5, 0.5, 0.5, 0.5)
        Ellipse(pos=touch.pos, size=(10, 10))

    def on_touch_down(self, touch: MouseMotionEvent):
        self.send_event(0, touch.spos)
        with self.canvas:
            self.draw(touch)

    def on_touch_move(self, touch):
        self.send_event(1, touch.spos)
        with self.canvas:
            self.draw(touch)

    def on_touch_up(self, touch):
        self.send_event(2, touch.spos)
        with self.canvas:
            self.draw(touch)

    def send_event(self, click_type: int, pos):
        self.conn.send(int(click_type).to_bytes(4, byteorder='big', signed=True))
        self.conn.send(int(pos[0] * self.resolution).to_bytes(4, byteorder='big', signed=True))
        self.conn.send(int(pos[1] * self.resolution).to_bytes(4, byteorder='big', signed=True))



class SimpleKivy4(App):

    def __init__(self, conn, resolution, **kwargs):
        super().__init__(**kwargs)
        self.conn = conn
        self.resolution = resolution

    def build(self):
        return TouchInput(self.conn, self.resolution)


if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket:
        socket.connect(("192.168.178.86", 1337))
        SimpleKivy4(socket, 100000).run()
