import socket
from time import sleep

import mouse
import pyautogui

if __name__ == "__main__":
    RESOLUTION = 100000
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket:
        socket.bind(("0.0.0.0", 1337))
        socket.listen(1)
        conn, addr = socket.accept()
        with conn:
            while True:
                type = int.from_bytes(conn.recv(4), byteorder='big', signed=True)
                x = int.from_bytes(conn.recv(4), byteorder='big', signed=True)
                y = int.from_bytes(conn.recv(4), byteorder='big', signed=True)

                width, height = pyautogui.size()

                x = width * (x / RESOLUTION)
                y = height * (1 - (y / RESOLUTION))

                mouse.move(x, y, True, 0)
                match type:
                    case 0:
                        pyautogui.mouseDown()
                    case 2:
                        pyautogui.mouseUp()

                print(type, x, y)
                sleep(0.02)
